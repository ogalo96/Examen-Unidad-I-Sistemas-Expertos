/*jshint esversion: 6 */
var db; //variable global
var colum = 1;
(function () {
    if (!('indexedDB' in window)) {
        console.err("El navegador no soporta indexedDB");
        return;
    }

    var solicitud = window.indexedDB.open("facebook", 1); //Parametros: nombre, version. La version debe ser entero

    //Se ejecutara en caso de que pueda abrir la BD sin problemas
    solicitud.onsuccess = function (evento) {
        console.log("Se abrio la base de datos");
        db = solicitud.result;
        //Leer la informacion del objectstore e imprimirla en la tabla html
        llenarSelectUsuarios();
        cargarPost();
    };


    //Se ejecutar en caso no se pueda abrir la base de datos
    solicitud.onerror = function (evento) {
        console.err("No se pudo abrir la base datos");
    };

    function llenarSelectUsuarios() {
        //Leer el objectstore de usuarios para imprimir la informacion, debe ser en este punto porque esta funcion se ejecuta si se abrio la BD correctamente
        var transaccion = db.transaction(["usuarios"], "readonly"); ///readwrite: Escritura/lectura, readonly: Solo lectura
        var objectStoreUsuarios = transaccion.objectStore("usuarios");
        var cursor = objectStoreUsuarios.openCursor();
        cursor.onsuccess = function (evento) {
            //Se ejecuta por cada objeto del objecstore
            if (evento.target.result) {
                console.log(evento.target.result.value);
                document.getElementById("sle-usuarios").innerHTML +=
                    `
                <option>
                ${evento.target.result.value.firstname} ${evento.target.result.value.lastname}
                </option>
                `;

                evento.target.result.continue();
            }
        };
    }

})();

function crearPost() {
    if (
        validarCampoVacio("sle-usuarios") &&
        validarCampoVacio("txt-contenido") &&
        validarCampoVacio("txt-fecha") 
    ){
        var post = {};
        post.usuario =  document.getElementById("sle-usuarios").value;
        post.post =  document.getElementById("txt-contenido").value;
        post.fecha =  document.getElementById("txt-fecha").value;
        
        ///Guardar informacion en el objectstore de post de la base de datos de facebook
        var transaccion = db.transaction(["post"],"readwrite");///readwrite: Escritura/lectura, readonly: Solo lectura
        var objectStorePost = transaccion.objectStore("post");
        var solicitud = objectStorePost.add(post);
        solicitud.onsuccess = function(evento){
            console.log("Se agrego el post correctamente");
           añadirAColum(post);
           
        };

        solicitud.onerror = function(evento){
            console.log("Ocurrio un error al guardar");
        };

        console.log(post);
    }
}


function validarCampoVacio(id){
    if (document.getElementById(id).value==""){
        document.getElementById(id).classList.remove("is-valid");
        document.getElementById(id).classList.add("is-invalid");
        return false;
    } else{
        document.getElementById(id).classList.remove("is-invalid");
        document.getElementById(id).classList.add("is-valid");
        return true;
    }
}
function añadirAColum(post){
    var columna = document.getElementById(`cl-${colum}`);
            d = 
            crearTarjeta(post);
            columna.innerHTML += d;
            
            
            colum +=1;
            if(colum > 3){
                colum = 1;
            }

}
function cargarPost(){
    var transaccion = db.transaction(["post"],"readonly");///readwrite: Escritura/lectura, readonly: Solo lectura
    var objectStorePost = transaccion.objectStore("post");
    var cursor = objectStorePost.openCursor();
    var d;
    cursor.onsuccess = function(evento){
        //Se ejecuta por cada objeto del objecstore
        var datos = evento.target.result;
        if(datos){
            añadirAColum(datos.value);
            datos.continue();
        }
    };
  
}

function convFecha(fecha){
    fecha = fecha.split("-");
    switch (fecha[1]) {
        case '01':
            fecha[1] = 'Enero';
            break;
            case '02':
            fecha[1] = 'Febrero';
            break;
            case '03':
            fecha[1] = 'Marzo';
            break;
            case '04':
            fecha[1] = 'Abril';
            break;
            case '05':
            fecha[1] = 'Mayo';
            break;
            case '06':
            fecha[1] = 'Junio';
            break;
            case '07':
            fecha[1] = 'Julio';
            break;
            case '08':
            fecha[1] = 'Agosto';
            break;
            case '09':
            fecha[1] = 'Septiembre';
            break;
            case '10':
            fecha[1] = 'Octubre';
            break;
            case '11':
            fecha[1] = 'Noviembre';
            break;
            case '12':
            fecha[1] = 'Diciembre';
            break;
        default:
            break;
    }
    fecha = fecha.reverse().join('/');
    return fecha;
}

function crearTarjeta(post) {
    var card = `
    <div class="card mb-2">
    <div class="card-body">
        <div class="card-title mb-4 font-weight-bold">
        <div class="media">
            <img src="img/profile.jpg" class="rounded-circle img-thumbnail user-img ">
            <div class="align-self-center">${post.usuario}
            <span class="text-muted">(${convFecha(post.fecha)})</span>
            </div>
        </div>
        </div>
        <hr class="card-subtitle mb-2 text-muted">
        <p class="card-text">${post.post}</p>
    </div>
    </div>
    `;
    return card;
}